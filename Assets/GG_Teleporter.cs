﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GG_Teleporter : MonoBehaviour
{

    private Vector3 entryVelocity;

    [Tooltip("Select the exit position child object of a different teleporter")]
    public Transform exitTeleportPosition;

    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == GG_TagManager.BALL)
        {
            Rigidbody rb = other.GetComponent<Rigidbody>();
            entryVelocity = rb.velocity;

            //ball direction 
            Vector3 dir = entryVelocity.normalized;

            //Speed
            float speed = entryVelocity.magnitude;

            Vector3 teleporterDirection = exitTeleportPosition.forward;
            Vector3 newVelocity = teleporterDirection.normalized * entryVelocity.magnitude;
            

            other.transform.position = exitTeleportPosition.position;

            rb.velocity = newVelocity;

        }
    }


}
