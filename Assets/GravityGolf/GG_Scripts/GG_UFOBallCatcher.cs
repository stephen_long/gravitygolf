﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using System;

public class GG_UFOBallCatcher : MonoBehaviour
{

    private GG_UFOPatroller ufo; //The ufo this belongs to

    [SerializeField]
    private Transform ballCurrentlyPickedUp;
    private void Start()
    {
        ufo = transform.parent.GetComponent<GG_UFOPatroller>();
    }
    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == GG_TagManager.BALL)
        {
            ufo.ballPickedUp = true;
            ufo.stayStatic = true;
            other.transform.parent = transform;
            other.transform.GetComponent<Rigidbody>().useGravity = false;
            other.transform.GetComponent<Rigidbody>().velocity = Vector3.zero;
            ballCurrentlyPickedUp = other.transform;
            other.transform.DOMove(transform.position, 2.0f).OnComplete(
                PickUpBallComplete
                );
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.tag == GG_TagManager.BALL)
        {
            ufo.ballPickedUp = false;
        }
    }

    private void PickUpBallComplete()
    {

        ufo.MoveToDropZone();
    }

    public void DropBall()
    {
        ufo.ballPickedUp = false;
        GetComponent<MeshCollider>().enabled = false;
        GetComponent<MeshRenderer>().enabled = false;
        Debug.LogError("Drop ball called");
        ballCurrentlyPickedUp.GetComponent<Rigidbody>().useGravity = true;
        ballCurrentlyPickedUp.parent = null;

        StartCoroutine(ReenableRay());
        
    }

    IEnumerator ReenableRay()
    {
        yield return new WaitForSeconds(3.0f);

        GetComponent<MeshCollider>().enabled = true;
        GetComponent<MeshRenderer>().enabled = true;
    }
}
