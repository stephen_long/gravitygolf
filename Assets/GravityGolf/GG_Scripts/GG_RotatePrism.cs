﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class GG_RotatePrism : MonoBehaviour
{
    public float speed = 5.0f;
    private void Update()
    {
        transform.DORotate(transform.rotation.eulerAngles + Vector3.forward * speed, .25f).SetLoops(-1, LoopType.Incremental);
    }
}
