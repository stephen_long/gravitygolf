﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GG_ColourArchController : MonoBehaviour
{
    public GG_ColourArch arch;

    private void Start()
    {
        transform.GetComponent<MeshRenderer>().material = arch.archMaterial;
    }
    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == GG_TagManager.BALL)
        {
            GravityControl gc = other.GetComponent<GravityControl>();
            gc.transform.GetComponent<MeshRenderer>().material = arch.archMaterial;

            foreach(GG_ColourArch ac in gc.archControllers)
            {
                if (ac.archMaterial == gc.GetComponent<MeshRenderer>().sharedMaterial)
                {
                    ac.GetComponent<Collider>().isTrigger = true;
                }
                else {
                    ac.GetComponent<Collider>().isTrigger = false;
                }
            }
        }
    }

    private void OnDrawGizmos()
    {
        Gizmos.DrawLine(transform.position, arch.transform.position);
    }
}
