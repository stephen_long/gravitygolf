﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GG_AirflowTube : MonoBehaviour
{

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == GG_TagManager.BALL)
        {
            Vector3 forceVector = transform.position - other.transform.position;

            other.GetComponent<Rigidbody>().AddForce(forceVector * 30);
        }
    }

    private void OnTriggerStay(Collider other)
    {
        if(other.tag == GG_TagManager.BALL)
        {
            other.GetComponent<Rigidbody>().AddForce(transform.up * 15);
        }
        
    }
}
