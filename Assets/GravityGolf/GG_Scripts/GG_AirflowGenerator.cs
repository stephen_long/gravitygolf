﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GG_AirflowGenerator : MonoBehaviour
{

    GameObject airflowArea;
    ParticleSystem ps;
    [SerializeField]
    bool airflowActive = true;


    public float onDuration;
    public float offDuration;
    public float force = 15f;

    // Start is called before the first frame update
    void Start()
    {
        airflowArea = transform.GetChild(0).gameObject;
        ps = GetComponentInChildren<ParticleSystem>();


        StartCoroutine(ToggleAirFlowState());
        //InvokeRepeating("SetAirflowState", 5.0f, 5.0f);
    }



   IEnumerator ToggleAirFlowState()
    {
        while (airflowActive)
        {


            //Turn on air
            airflowArea.SetActive(true);



            //wait for duration 
            yield return new WaitForSeconds(onDuration);

            //turn off air
            airflowArea.SetActive(false);

            airflowActive = false;
            yield return new WaitForSeconds(offDuration);
            airflowActive = true;
        }
    }

    /// <summary>
    /// Toggles the flow of air on/off
    /// </summary>
    void SetAirflowState()
    {
        airflowActive = !airflowActive;
        Debug.Log("Airflow active set to: " + airflowActive);

        airflowArea.SetActive(!airflowArea.activeInHierarchy);
    }

    private void OnTriggerStay(Collider other)
    {
        if(other.tag == GG_TagManager.BALL && airflowActive)
        {
            other.GetComponent<Rigidbody>().AddForce(transform.forward * force);
        }
    }

}
