﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GG_UFOPatroller : MonoBehaviour
{
    public bool stayStatic = false;
    public List<Transform> waypoints = new List<Transform>();
    public Transform dropZone; //The zone the UFO will drop the ball if it catches it

    private Transform targetWaypoint;
    [SerializeField]
    private int targetWaypointIndex = 0;
    private float movementSpeed = 1.5f;
    private GG_UFOBallCatcher ufoRay;
    private bool movingToDropZone = false;

    public bool ballPickedUp = false;
    // Start is called before the first frame update
    void Awake()
    {
        targetWaypoint = waypoints[targetWaypointIndex];
        ufoRay = GetComponentInChildren<GG_UFOBallCatcher>();
    }

    // Update is called once per frame
    void Update()
    {
        if(stayStatic)
        {
            return;
        }
        float movementStep = Time.deltaTime * movementSpeed;
        transform.position = Vector3.MoveTowards(transform.position, targetWaypoint.position, movementStep);


        CheckWaypointReached();

    }

    private void CheckWaypointReached()
    {
        if (Vector3.Distance(transform.position, targetWaypoint.position) < 0.2f)
        {
            if(ballPickedUp)
            {
                ufoRay.DropBall();
            }

            SetNextWaypoint();
        }
    }

    private void SetNextWaypoint()
    {
        targetWaypointIndex++;

        if(targetWaypointIndex >= waypoints.Count)
        {
            targetWaypointIndex = 0;
        }

        targetWaypoint = waypoints[targetWaypointIndex];
    }

    public void MoveToDropZone()
    {
        stayStatic = false;

        targetWaypoint = dropZone;
        targetWaypointIndex = -1;
    }
}
