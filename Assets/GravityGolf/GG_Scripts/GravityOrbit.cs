﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GravityOrbit : MonoBehaviour
{

    public float gravity;


    private void OnTriggerEnter(Collider other)
    {
        if(other.GetComponent<GravityControl>())
        {
            other.GetComponent<GravityControl>().gravity = this.GetComponent<GravityOrbit>();
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.GetComponent<GravityControl>())
        {
            other.GetComponent<GravityControl>().gravity = null;
        }
    }
}
