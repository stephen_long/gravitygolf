﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GG_TrapDoorController : MonoBehaviour
{

    public Animator anim; //The trapdoor animator component


    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == GG_TagManager.BALL)
        {
            OpenTrapDoor();
        }
    }

    private void OpenTrapDoor()
    {
        anim.SetTrigger("Open");

        foreach(BoxCollider bc in GetComponents<BoxCollider>())
        {
            bc.enabled = false;
        }
    }
}
