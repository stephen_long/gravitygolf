﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GravityControl : MonoBehaviour
{

    public GravityOrbit gravity;
    private Rigidbody rb;

    public float rotationSpeed = 20;


    public Vector3 lastHitPosition;
    public Vector3 lastHitRotation;

    public float ballCurrentMoveSpeed;

    public float force; //Only used for test hitting a shot with the ApplyForce function



    private MeshRenderer meshRenderer;
    public List<GG_ColourArch> archControllers = new List<GG_ColourArch>();

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        meshRenderer = GetComponent<MeshRenderer>();
        archControllers = new List<GG_ColourArch>(FindObjectsOfType<GG_ColourArch>());
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        ballCurrentMoveSpeed = rb.velocity.magnitude;
        if (Input.GetKey(KeyCode.A))
        {
            transform.Rotate(transform.up, -2f);
        }

        if (Input.GetKey(KeyCode.D))
        {
            transform.Rotate(transform.up, 2f);
        }

        if (Input.GetKeyDown(KeyCode.Space))
        {
            if(rb.velocity.magnitude < 0.1f)
            ApplyForce();
        }

        Debug.DrawRay(transform.position, this.transform.forward * 10f, Color.blue);
        if(gravity)
        {
            Vector3 gravityUp = Vector3.zero;

            gravityUp = (transform.position - gravity.transform.position).normalized;

            Vector3 localUp = transform.up;

            Quaternion targetRotation = Quaternion.FromToRotation(localUp, gravityUp) * transform.rotation;

            transform.up = Vector3.Lerp(transform.up, gravityUp, rotationSpeed * Time.deltaTime);

            float dist = Vector3.Distance(transform.position, gravity.transform.position);
            

            rb.AddForce((-gravityUp * (gravity.gravity/dist)) * rb.mass);
        }
    }

    void ApplyForce()
    {
        lastHitPosition = transform.position;
        ;lastHitRotation = transform.eulerAngles;

        rb.AddForce(transform.forward * force); //520 default
    }

    public void ResetBall()
    {
        rb.velocity = Vector3.zero;
        rb.angularVelocity = Vector3.zero;

        transform.position = lastHitPosition;
        transform.eulerAngles = lastHitRotation;
    }
}
