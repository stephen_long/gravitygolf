﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GG_BallBooster : MonoBehaviour
{

    public enum BoostDirection { 
        UP,
        FORWARD
    };

    public BoostDirection boostDirection;

    public float boosterStrength = 500f;
    private void OnDrawGizmos()
    {
        Gizmos.DrawWireCube(transform.position, transform.localScale);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == GG_TagManager.BALL)
        {
            Rigidbody rb = other.GetComponent<Rigidbody>();

            if (boostDirection == BoostDirection.FORWARD)
            {
                rb.AddForce(transform.forward * boosterStrength);
            }
            else
            {
                rb.AddForce(Vector3.up * boosterStrength);
                rb.AddForce(transform.forward * boosterStrength / 2);
            }
        }




            
    }
}
