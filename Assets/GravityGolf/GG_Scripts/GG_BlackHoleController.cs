﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GG_BlackHoleController : MonoBehaviour
{

    public Transform ball;
    public float distance;
    private void OnTriggerEnter(Collider other)
    {
        if (other.GetComponent<GravityControl>())
        {
            other.GetComponent<GravityControl>().ResetBall();
        }
    }

    private void Update()
    {
        distance = Mathf.Min(Mathf.Max(Vector3.Distance(transform.position, ball.position), 0), 5) - 0;
    }


}
