﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;


public class GG_RotatePlatform : MonoBehaviour
{
    public enum Direction
    {
        X,
        Y,
        Z
    };

    public Direction direction;

    

    // Start is called before the first frame update
    void Start()
    {

        Sequence seq = DOTween.Sequence();


        if (direction == Direction.X)
        {
            seq.Append(transform.DORotate(Vector3.right * 90f, .25f, RotateMode.Fast)).SetLoops(-1, LoopType.Incremental).AppendInterval(2.0f).SetRelative().SetUpdate(UpdateType.Fixed);

        }

        else if (direction == Direction.Y)
        {
            seq.Append(transform.DORotate(Vector3.up * 90f, .25f, RotateMode.Fast)).SetLoops(-1, LoopType.Incremental).AppendInterval(2.0f).SetRelative().SetUpdate(UpdateType.Fixed);
        }

        else if (direction == Direction.Z)
        {
            seq.Append(transform.DORotate(-Vector3.forward * 90f, .25f, RotateMode.Fast)).SetLoops(-1, LoopType.Incremental).AppendInterval(2.0f).SetRelative().SetUpdate(UpdateType.Fixed);
        }

    }

}
