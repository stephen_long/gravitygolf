﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Oculus;
using System;

public class GG_Player : MonoBehaviour
{
     Transform ball;
    public Transform hand;
    // Start is called before the first frame update
    void Start()
    {
        ball = GameObject.FindGameObjectWithTag(GG_TagManager.BALL).transform;
    }

    // Update is called once per frame
    void Update()
    {
        if(OVRInput.GetDown(OVRInput.Button.One))
        {
            ResetBall();
        }

        if (OVRInput.GetDown(OVRInput.Button.Two))
        {
            StartCoroutine(Teleport());
        }
    }

    private void ResetBall()
    {
        Debug.LogError("X presssed");
        ball.GetComponent<Rigidbody>().velocity = Vector3.zero;
        ball.GetComponent<Rigidbody>().angularVelocity = Vector3.zero;
        ball.transform.position = hand.position;
    }

    private void TeleportToBall()
    {
        Vector3 teleportedPos = new Vector3(ball.position.x, ball.position.y + 2.5f, ball.position.z);
        transform.localPosition = teleportedPos;
    }

    IEnumerator Teleport()
    {
        GetComponent<CharacterController>().enabled = false;
        GetComponent<OVRPlayerController>().enabled = false;

        TeleportToBall();

        yield return new WaitForSeconds(1.0f);

        GetComponent<CharacterController>().enabled = true;
        GetComponent<OVRPlayerController>().enabled = true;

    }
}
