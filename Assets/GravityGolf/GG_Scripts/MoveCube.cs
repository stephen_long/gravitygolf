﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;


public class MoveCube : MonoBehaviour
{
    public float distance;
    public float timeToReach;
    public enum Direction { 
        X,
        Y,
        Z
    };

    public Direction direction;

    // Start is called before the first frame update
    void Start()
    {

        Sequence seq = DOTween.Sequence();

        seq.Append(transform.DORotate(Vector3.right * 90f, .25f, RotateMode.Fast)).SetLoops(-1, LoopType.Incremental).AppendInterval(2.0f).SetRelative().SetUpdate(UpdateType.Fixed);

        //if(direction == Direction.X)
        //{
        //    seq.SetLoops(-1, LoopType.Yoyo);
        //    seq.SetEase(Ease.InOutSine);
        //    seq.Append(transform.DOMoveX(distance, timeToReach, false));



        //}

        //if (direction == Direction.Y)
        //{
        //    transform.DOMoveY(distance, timeToReach, false).SetLoops(-1, LoopType.Yoyo).SetEase(Ease.Linear);
        //}

        //if (direction == Direction.Z)
        //{
        //    transform.DOMoveZ(distance, timeToReach, false).SetLoops(-1, LoopType.Yoyo).SetEase(Ease.Linear);
        //}
     
        

    }


}
