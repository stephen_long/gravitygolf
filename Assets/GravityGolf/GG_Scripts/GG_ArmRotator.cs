﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class GG_ArmRotator : MonoBehaviour
{


    private Vector3 startRot;
    public float speed = 2.0f;

    public int rotationAmountZ = 75;
    // Start is called before the first frame update
    void Start()
    {

        Sequence mySequence = DOTween.Sequence();

        mySequence.Append(transform.DORotate(new Vector3(0, 0, rotationAmountZ), speed));

        mySequence.SetLoops(-1, LoopType.Yoyo);
    }

    // Update is called once per frame
    void Update()
    {
        

    }
}
