﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GG_MovingPrism : MonoBehaviour
{

    public float speed = .1f;
    public Transform waypoint1;
    public Transform waypoint2;
    
    private float elapsedTime = 0.0f;


    private void Start()
    {
        
    }

    public void FixedUpdate()
    {
        elapsedTime += Time.deltaTime;
        float cosineValue = Mathf.Cos(2.0f * Mathf.PI * speed * elapsedTime);
        transform.position = waypoint1.position + (waypoint2.position - waypoint1.position) * 0.5f * (1 - cosineValue);
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.blue;
        Gizmos.DrawRay(waypoint1.position, DirectionToWaypoint() * DistanceBetweenPoints());
    }

    float DistanceBetweenPoints()
    {
        return Vector3.Distance(waypoint1.position, waypoint2.position);
    }

    Vector3 DirectionToWaypoint()
    {
        return waypoint2.position - waypoint1.position;
    }
}
