﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GG_DollyCarrier : MonoBehaviour
{

    public Cinemachine.CinemachineDollyCart dollyCart;

    private Transform ball;
    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == GG_TagManager.BALL)
        {
            ball = other.transform;
            other.transform.GetComponent<Rigidbody>().useGravity = false;
            other.transform.GetComponent<Rigidbody>().velocity = Vector3.zero;
            other.transform.parent = transform;


            MoveDolly();
        }
    }

    private void Update()
    {
        if(dollyCart.m_Position == 1)
        {
            DropBall();
        }
    }

    private void DropBall()
    {
        ball.transform.parent = null;
        ball.GetComponent<Rigidbody>().useGravity = true;
    }

    private void MoveDolly()
    {
        
        dollyCart.m_Speed = 0.3f;
    }
}
