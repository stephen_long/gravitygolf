﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GG_BallReseter : MonoBehaviour
{

    public Vector3 resetPos;
    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == GG_TagManager.BALL)
        {
            other.GetComponent<Rigidbody>().velocity = Vector3.zero;
            other.GetComponent<Rigidbody>().angularVelocity = Vector3.zero;

            other.transform.position = resetPos;
        }
    }
}
